# Maintainer: Carlo Landmeter <clandmeter@alpinelinux.org>
pkgname=expat
pkgver=2.4.6
pkgrel=0
pkgdesc="XML Parser library written in C"
url="https://libexpat.github.io/"
arch="all"
license='MIT'
checkdepends="bash"
source="https://downloads.sourceforge.net/project/expat/expat/$pkgver/expat-$pkgver.tar.bz2"
subpackages="$pkgname-static $pkgname-dev $pkgname-doc"

# secfixes:
#   2.4.5-r0:
#     - CVE-2022-25235
#     - CVE-2022-25236
#     - CVE-2022-25313
#     - CVE-2022-25314
#     - CVE-2022-25315
#   2.4.4-r0:
#     - CVE-2022-23852
#     - CVE-2022-23990
#   2.4.3-r0:
#     - CVE-2021-45960
#     - CVE-2021-46143
#     - CVE-2022-22822
#     - CVE-2022-22823
#     - CVE-2022-22824
#     - CVE-2022-22825
#     - CVE-2022-22826
#     - CVE-2022-22827
#   2.2.7-r1:
#     - CVE-2019-15903
#   2.2.7-r0:
#     - CVE-2018-20843
#   2.2.0-r1:
#     - CVE-2017-9233

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--enable-static
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir/" install
}

sha512sums="
87e9e9597867fc73352122b89ea3e1d0dba0d81640b1defd7633bb13108b89e1703b69358021b90f0af29854020ddbc07bf56ea6acc764e5bdecd51ee6050d99  expat-2.4.6.tar.bz2
"
