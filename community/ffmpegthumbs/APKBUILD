# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ffmpegthumbs
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by kio
arch="all !armhf !s390x !riscv64"
url="https://www.kde.org/applications/multimedia/"
pkgdesc="FFmpeg-based thumbnail creator for video files"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	ffmpeg-dev
	kconfig-dev
	ki18n-dev
	kio-dev
	qt5-qtbase-dev
	taglib-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/ffmpegthumbs-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
433f5487b13e665898cf30a5f49e4d7971d9225d0ecd8b061fcd17310db87506b8e1ac36d56e67d483d16cd2b915e4a5a4b488bddb7186a054c7c3ef1c945062  ffmpegthumbs-21.12.2.tar.xz
"
