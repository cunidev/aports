# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=katomic
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kxmlgui
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/games/org.kde.katomic"
pkgdesc="A fun and educational game built around molecular geometry"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	qt5-qtbase-dev
	kcoreaddons-dev
	kconfig-dev
	kcrash-dev
	kwidgetsaddons-dev
	ki18n-dev
	kxmlgui-dev
	knewstuff-dev
	kdoctools-dev
	kdbusaddons-dev
	libkdegames-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/katomic-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
962a817e9c5302a900e61af6d64edc833a90059f529976217a7990a0b97d0b9fffccc6f952ce1eafda623758109bb1a5a6b50b8875e1406d39e7ba1b22c1b12c  katomic-21.12.2.tar.xz
"
