# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=milou
pkgver=5.24.2
pkgrel=0
pkgdesc="A dedicated search application built on top of Baloo"
# armhf blocked by qt5-qtdeclarative
# s390x blocked by polkit -> kdeclarative
arch="all !armhf !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later AND (GPL-2.0-only OR GPL-3.0-only) AND (LGPL-2.1-only OR LGPL-3.0-only)"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	kdeclarative-dev
	ki18n-dev
	kitemmodels-dev
	krunner-dev
	kservice-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/milou-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
bda9b8678afdbf66641e2034f2f9e763d988fb175c70496e728612596518f80a88ab6f138439d1be099bc637bb903874480470b9a341fa52a58de3a3ee842c13  milou-5.24.2.tar.xz
"
