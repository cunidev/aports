# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=pimcommon
pkgver=21.12.2
pkgrel=0
pkgdesc="Common lib for KDEPim"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
# ppc64le blocked by qt5-qtwebengine -> akonadi
arch="all !armhf !s390x !riscv64 !ppc64le"
url='https://community.kde.org/KDE_PIM'
license="GPL-2.0-or-later"
depends_dev="
	akonadi-contacts-dev
	akonadi-dev
	karchive-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcontacts-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kimap-dev
	kio-dev
	kitemmodels-dev
	kjobwidgets-dev
	kmime-dev
	knewstuff-dev
	kpimtextedit-dev
	kservice-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdepim-dev
	purpose-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	qt5-qttools-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/pimcommon-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="net" # net required for tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_DESIGNERPLUGIN=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
8dff1516772bafcf9a4bad233645b940ecfd07cb2a31869be6dc9f48c6448930bbbd9eedb5d742d0d1b32674988f6f41a20a8aa155df519d100630a4ad85c7a4  pimcommon-21.12.2.tar.xz
"
