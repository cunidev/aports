# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libktorrent
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/internet/org.kde.ktorrent"
pkgdesc="A powerful BitTorrent client for KDE"
license="GPL-2.0-or-later"
depends_dev="
	boost-dev
	gmp-dev
	karchive-dev
	kcrash-dev
	ki18n-dev
	kio-dev
	qca-dev
	qt5-qtbase-dev
	solid-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/libktorrent-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# utppolltest requires network access
	# superseedtest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(utppoll|superseed)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
930b256b2bdbce0950f3473b7d9f9dec241207c32a72ac9cd6d7e5094f8a6c5ed1e36368c8120a8d467540b064da850bdd714b79fe3a5f2971e3c5e8d7a6a743  libktorrent-21.12.2.tar.xz
"
