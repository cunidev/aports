# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kross
pkgver=5.91.0
pkgrel=0
pkgdesc="Framework for scripting KDE applications"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit
arch="all !armhf !s390x !riscv64"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	kcompletion-dev
	kcoreaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kparts-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt5-qtbase-dev
	qt5-qttools-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	kdoctools-dev
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kross-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
394ab4049b72fb514a7311142bc084d8ac060851e3f23fb350ccb502e08092ac4d9eb308ceebc14d9289e5604cd6f9a015fd0eb82fcca77ab202a2108aa2f3c2  kross-5.91.0.tar.xz
"
