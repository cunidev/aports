# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=itinerary
pkgver=21.12.2
pkgrel=0
# armhf blocked by qt5-qtdeclarative
# ppc64le blocked by kitinerary
# s390x blocked by qt5-qtdeclarative
arch="aarch64 armv7 x86_64 x86"
url="https://invent.kde.org/pim/itinerary"
pkgdesc="Itinerary and boarding pass management application"
license="BSD-3-Clause AND LGPL-2.0-or-later"
depends="
	kirigami2
	prison
	qt5-qtlocation
	tzdata
	"
makedepends="
	extra-cmake-modules
	kcontacts-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kholidays-dev
	ki18n-dev
	kitinerary-dev
	knotifications-dev
	kosmindoormap-dev
	kpkpass-dev
	kpublictransport-dev
	networkmanager-qt-dev
	qqc2-desktop-style-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtlocation-dev
	qt5-qtquickcontrols2-dev
	shared-mime-info
	solid-dev
	zlib-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/itinerary-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# tripgrouptest and timelinemodel test are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "(tripgroup|timelinemodel)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
aa30f9e584f283627ceba6eef56fea068490feeb667e384546df0a8abde644f12c478170c523ac5db3056ab84a39afc4bd157faced8a2f4472f4f51c570a41fe  itinerary-21.12.2.tar.xz
"
